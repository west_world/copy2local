package handler

import (
	"encoding/json"
	"net/http"
	"bytes"
	"net/url"
	"io/ioutil"
	"fmt"
	"path"
	"strings"
	"os"
	"strconv"
)

type C2LWrapper struct {
	ThreadNum  int
	IPFileAddr string
	IP         string
	Port       int
	ProxyIP    string
	ProxyPort  int
	Addr       string
	LocalAddr string
}

// 若 ProxyUrl 为空则直接向 TargetUrl 发起请求
type UrlWrapper struct {
	TargetUrl string
	ProxyUrl  string
}

type C2LReq struct {
	Addr string `json:"addr"`
}

func (w *C2LWrapper) Execute() {

	httpPre := "http://"
	urlWrapper := UrlWrapper{}
	urlWrapper.TargetUrl = fmt.Sprintf("%v%v:%v/download", httpPre, w.IP, w.Port)
	if len(w.ProxyIP) != 0 {
		urlWrapper.ProxyUrl = fmt.Sprintf("%v%v:%v", httpPre, w.ProxyIP, w.ProxyPort)
	}

	resp, err := Post(&urlWrapper, w.Addr)
	if err != nil {
		fmt.Printf("err:%v\n", err)
		return
	}

	fmt.Printf("code:%v\n", resp.Code)
	fmt.Printf("msg:%v\n", resp.Msg)

	if resp.Code != 200 {
		return
	}

	fileName := path.Base(w.Addr)
	if len(w.LocalAddr) == 0{
		w.LocalAddr="./"
	}

	if strings.HasSuffix(w.LocalAddr, "/"){
		w.LocalAddr += fileName
	}

	fo, err := os.Create(w.LocalAddr)
	if err != nil{
		fmt.Printf("err:%v\n", err)
		return
	}
	defer func() {
		if err := fo.Close(); err != nil {
			fmt.Printf("err:%v\n", err)
			return
		}
	}()

	fo.Write(resp.Bytes)
}

type C2LResp struct {
	Code   int
	Msg    string
	Bytes []byte
}

func Post(urlWrapper *UrlWrapper, addr string) (resp *C2LResp, err error) {
	req := C2LReq{}
	req.Addr = addr
	postJson, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}

	postReq, err := http.NewRequest("POST", urlWrapper.TargetUrl, bytes.NewBuffer(postJson))
	if err != nil {
		return nil, err
	}

	postReq.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	if len(urlWrapper.ProxyUrl) != 0 {
		proxyUrl, _ := url.Parse(urlWrapper.ProxyUrl)
		transport := &http.Transport{}
		transport.Proxy = http.ProxyURL(proxyUrl)
		client.Transport = transport
	}

	postResp, err := client.Do(postReq)
	if err != nil {
		return nil, err
	}
	defer postReq.Body.Close()

	r := &C2LResp{}
	code ,err := strconv.Atoi(postResp.Header.Get("code"))
	if err != nil {
		return  nil, err
	}
	r.Code = code
	r.Msg = postResp.Header.Get("msg")
	body, _ := ioutil.ReadAll(postResp.Body)
	r.Bytes = body
	return r, nil
}
