## commit note 2018.06.10

copy2local 客户端功能如下

- 直接指定 bagent 的 IP，执行命令
e.g. ./copy2local -i 127.0.0.1 -p 8090 -d /tmp/test -l ./test.bak
- 只需要制定 IP 和端口号，"http://" 和 url 路径程序会自动填充
- -l 后的路径如果包含文件名，则会覆盖；若不包含文件名则会根据请求的文件创建新文件
- 发送请求时，是发送的 post 命令，e.g.: {"addr":"/tmp/test"}，请求返回是一普通 http response， code 和 msg 在 http header 中，文件内容在 body 中