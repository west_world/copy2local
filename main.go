package main

import (
	"flag"
	"fmt"
	"os"
	
	"copy2local/handler"
)

func main() {

	c2lWrapper := handler.C2LWrapper{}

	flag.IntVar(&c2lWrapper.ThreadNum, "t", 1, "command executor thread num")
	flag.StringVar(&c2lWrapper.IPFileAddr, "f", "", "IPs file")
	flag.StringVar(&c2lWrapper.IP, "i", "", "IP address of bagent")
	flag.IntVar(&c2lWrapper.Port, "p", -1, "port of bagent")
	flag.StringVar(&c2lWrapper.ProxyIP, "x", "", "IP addr of proxy")
	flag.IntVar(&c2lWrapper.ProxyPort, "P", -1, "port addr of proxy")
	flag.StringVar(&c2lWrapper.Addr, "d", "", "addr of file in bagent")
	flag.StringVar(&c2lWrapper.LocalAddr, "l", "", "addr of local file")
	flag.Parse()

	//if len(c2lWrapper.IP) == 0 && len(c2lWrapper.IPFileAddr) == 0 {
	if len(c2lWrapper.IP) == 0 {
		fmt.Printf("IP or an IP file should be given")
		os.Exit(1)
	}
	if len(c2lWrapper.IP) != 0 && c2lWrapper.Port == -1 {
		fmt.Printf("the port should be given")
		os.Exit(1)
	}

	if len(c2lWrapper.ProxyIP) != 0 && c2lWrapper.ProxyPort == -1 {
		fmt.Printf("the port of proxy should be given")
		os.Exit(1)
	}

	if len(c2lWrapper.Addr) == 0 {
		fmt.Printf("no addr of file, exit")
		os.Exit(1)
	}

	if c2lWrapper.ThreadNum < 0 {
		fmt.Printf("thread num must > 0")
		os.Exit(1)
	}

 	c2lWrapper.Execute()

	os.Exit(0)
}
